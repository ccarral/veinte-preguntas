<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="mx.uaemex.fi.diseno.vp.model.data.Pregunta" %>
<%@page import="mx.uaemex.fi.diseno.vp.model.data.Item" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Juego</h1>
<form action="Respondiendo" method="post">
<select name="pregunta">
<%
	Item objetivo = (Item)session.getAttribute("objetivo");
	ArrayList<Pregunta> preguntas = (ArrayList<Pregunta>) session.getAttribute("preguntas");
	for(Pregunta p: preguntas){
		out.println("<option value=\""+p.getId()+"\">"+p.getTexto()+"</option>");
	}
%>
</select>
<input type="submit" value="Enviar">
</form>
</body>
</html>