package mx.uaemex.fi.diseno.vp.model;

import java.util.ArrayList;

import mx.uaemex.fi.diseno.vp.model.data.Item;
import mx.uaemex.fi.diseno.vp.model.data.Pregunta;
import mx.uaemex.fi.diseno.vp.model.data.PreguntaRespuesta;

public interface ItemsDAO {
		void crearItem(String c, ArrayList<PreguntaRespuesta> respuestas);
		void borrarItem(Item c);
		Item consultarItem (Item c);
		ArrayList<Item> consultar();
		boolean consultarRespuesta(Item c, Pregunta p);
		ArrayList<PreguntaRespuesta> consultarRespuestas(Item c);
		void modificarItem(Item cO, Item cN);
		void modificarRespuesta(Item c, PreguntaRespuesta r);
		void modificarRespuesta(Item c, ArrayList<PreguntaRespuesta> res);
}
