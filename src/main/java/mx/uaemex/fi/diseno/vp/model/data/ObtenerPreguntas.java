package mx.uaemex.fi.diseno.vp.model.data;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mx.uaemex.fi.diseno.vp.model.PreguntasDAO;
import mx.uaemex.fi.diseno.vp.model.PreguntasDAODerbyImp;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


//@WebServlet(description = "Vamos a obtener las preguntas, y añadir a la el espacio de la sesion", urlPatterns = { "consultarPreguntas" })
public class ObtenerPreguntas extends HttpServlet {
	private PreguntasDAO preguntasDao;
	private DataSource dataSource;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObtenerPreguntas() {
        super();
    }
    

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		InitialContext cxt;
		try {
			cxt = new InitialContext();
			if(cxt != null) {
				this.dataSource = (DataSource) cxt.lookup("java:/comp/env/jdbc/ds");
				if(this.dataSource == null) throw new ServletException("Data source not found!. ");
			}
		} catch (NamingException e) {
			throw new ServletException("No hay contexto inicial ... ");
		}

	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Pregunta> pp;
		HttpSession hps;
		
		PreguntasDAODerbyImp dao = new PreguntasDAODerbyImp();
		
		try {
			dao.setConexion(this.dataSource.getConnection());
			this.preguntasDao = dao;
			pp = this.preguntasDao.consultarPreguntas();
			hps= request.getSession();
			hps.setAttribute("preguntas", pp);
			response.sendRedirect("juego.jsp");
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
