package mx.uaemex.fi.diseno.vp.model.data;

public class Item {
	private int id;
	private String concepto;
	
	public Item() {
		
	}
	
	public Item(String c) {
		
		this.concepto = c;
		
	}
	
	public Item(int id, String c) {
		
		this.id = id;
		this.concepto = c;	
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	
	
	
	

}
