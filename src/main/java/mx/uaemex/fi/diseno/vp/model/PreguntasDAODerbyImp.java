package mx.uaemex.fi.diseno.vp.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import mx.uaemex.fi.diseno.vp.model.data.Pregunta;
import mx.uaemex.fi.diseno.vp.model.error.PersistenciaException;

public class PreguntasDAODerbyImp extends AbstractDerbyDAO implements PreguntasDAO {

	public PreguntasDAODerbyImp() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void crearPregunta(Pregunta p) {
		try {
			Statement stmt= this.conexion.createStatement();
			stmt.executeUpdate("INSERT INTO PREGUNTAS (PREGUNTA) VALUES ('"+p.getTexto()+"')");
		} catch (SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
		
	}

	@Override
	public void borrarPregunta(Pregunta p) {
		try {
			Statement stmt= this.conexion.createStatement();
			stmt.executeUpdate("DELETE FROM PREGUNTAS WHERE ID="+p.getId());
			
		} catch (SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	@Override
	public Pregunta consultarPregunta(Pregunta p) {
		ResultSet rs;
		Pregunta res = null;
		try {
			Statement stmt= this.conexion.createStatement();
			rs=stmt.executeQuery("SELECT * FROM PREGUNTAS WHERE ID="+p.getId());
			while(rs.next()) {
				res=new Pregunta();
				res.setId(rs.getInt("id"));
				res.setTexto(rs.getString("pregunta"));
			}
		} catch (SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
		return res;
	}

	@Override
	public ArrayList<Pregunta> consultarPreguntas() {
		ArrayList<Pregunta>resul;
		ResultSet rs;
		Pregunta p;
		try {
			Statement stmt= this.conexion.createStatement();
			rs=stmt.executeQuery("SELECT * FROM PREGUNTAS");
			resul= new ArrayList<Pregunta>();
			while(rs.next()) {
				p=new Pregunta();
				p.setId(rs.getInt("id"));
				p.setTexto(rs.getString("pregunta"));
				resul.add(p);
			}
		} catch (SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
		return resul;
	}

	@Override
	public void modificarPregunta(Pregunta pOri, Pregunta pNueva) {
		try {
			Statement stmt= this.conexion.createStatement();
			stmt.executeUpdate("UPDATE PREGUNTAS SET PREGUNTA='"+pNueva.getTexto()+"' WHERE ID="+pOri.getId());
		} catch (SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

}