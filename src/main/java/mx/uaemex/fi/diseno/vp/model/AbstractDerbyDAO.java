package mx.uaemex.fi.diseno.vp.model;

import java.sql.Connection;

public abstract class AbstractDerbyDAO {

	protected Connection conexion;
	
	public AbstractDerbyDAO() {
		
	}

	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}
}
