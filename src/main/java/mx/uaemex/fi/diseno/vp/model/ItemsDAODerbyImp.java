package mx.uaemex.fi.diseno.vp.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import mx.uaemex.fi.diseno.vp.model.data.Item;
import mx.uaemex.fi.diseno.vp.model.data.Pregunta;
import mx.uaemex.fi.diseno.vp.model.data.PreguntaRespuesta;
import mx.uaemex.fi.diseno.vp.model.error.PersistenciaException;

public class ItemsDAODerbyImp extends AbstractDerbyDAO implements ItemsDAO {

	public ItemsDAODerbyImp() {
	}

	@Override
	public void crearItem(String c, ArrayList<PreguntaRespuesta> respuestas) {
		try {
			Statement stmt = this.conexion.createStatement();
			stmt.executeUpdate("INSERT INTO ITEMS (CONCEPTO) VALUES ('"+c+"')", Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();
			if(rs.next()) {
				int conceptoID = rs.getInt(1);
				for(PreguntaRespuesta preguntaRespuesta : respuestas) {
						stmt.execute("INSERT INTO RESPUESTAS (CONCEPTO, PREGUNTA, RESPUESTA) VALUES ("+conceptoID+","+preguntaRespuesta.getPregunta().getId()+", "+preguntaRespuesta.isRespuesta()+")");	
				}
				
			}else {
				System.out.println("No se encontro el id!");
			}
			
		}catch(SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
		
	}

	@Override
	public void borrarItem(Item c) {
		try {
			Statement stmt = this.conexion.createStatement();
			stmt.execute("DELETE FROM ITEMS WHERE ID = "+c.getId());
		} catch (SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
		
	}

	@Override
	public Item consultarItem(Item c) {
		try {
			Statement stmt = this.conexion.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM ITEMS WHERE ID = "+c.getId());
			if(rs.next()) {
				int conceptoId = rs.getInt(1);
				String concepto = rs.getString(2);
				Item i = new Item();
				i.setConcepto(concepto);
				i.setId(conceptoId);
				return i;
			}
		} catch (SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
		return null;
	}

	@Override
	public ArrayList<Item> consultar() {
		try {
			Statement stmt = this.conexion.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM ITEMS");
			ArrayList<Item> items = new ArrayList();
			while(rs.next()) {
				int conceptoId = rs.getInt(1);
				String concepto = rs.getString(2);
				Item i = new Item();
				i.setConcepto(concepto);
				i.setId(conceptoId);
				items.add(i);
			}
			return items;
		} catch (SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	@Override
	public boolean consultarRespuesta(Item c, Pregunta p) {
		try {
			Statement stmt = this.conexion.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT * FROM RESPUESTAS WHERE CONCEPTO = "+c.getId()+"AND PREGUNTA = "+p.getId());
		
			if(rs.next()) {
				boolean respuesta = rs.getBoolean(4);
				return respuesta;
			}else {
				return false;
			}
		} catch (SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	@Override
	public ArrayList<PreguntaRespuesta> consultarRespuestas(Item c) {
		try {
			Statement stmt = this.conexion.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT PREGUNTAS.ID, PREGUNTAS.PREGUNTA, RESPUESTAS.RESPUESTA FROM PREGUNTAS INNER JOIN RESPUESTAS ON PREGUNTAS.ID = RESPUESTAS.PREGUNTA WHERE RESPUESTAS.CONCEPTO = "+c.getId());
			ArrayList<PreguntaRespuesta> items = new ArrayList();
			while(rs.next()) {
				PreguntaRespuesta pr = new PreguntaRespuesta();
				
				Pregunta p1 = new Pregunta();
				p1.setId(rs.getInt(1));
				p1.setTexto(rs.getString(2));
				pr.setPregunta(p1);
				pr.setRespuesta(rs.getBoolean(3));
				
				items.add(pr);

			}
			
			return items;
		} catch (SQLException e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	@Override
	public void modificarItem(Item cO, Item cN) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modificarRespuesta(Item c, PreguntaRespuesta r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modificarRespuesta(Item c, ArrayList<PreguntaRespuesta> res) {
		// TODO Auto-generated method stub
		
	}

}
