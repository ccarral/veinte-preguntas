package mx.uaemex.fi.diseno.vp.model;

import java.util.ArrayList;

import mx.uaemex.fi.diseno.vp.model.data.Pregunta;

public interface PreguntasDAO {
	void crearPregunta(Pregunta p);
	void borrarPregunta(Pregunta p);
	Pregunta consultarPregunta(Pregunta p);
	ArrayList<Pregunta> consultarPreguntas();
	void modificarPregunta(Pregunta pOri, Pregunta pNueva);
	

}
