package mx.uaemex.fi.diseno.vp.model.data;

public class Pregunta {
	private int id;
	private String texto;
	
	public Pregunta() {
		
	}
	
	public Pregunta(String p) {
		this.texto = p;
	}
	
	public Pregunta(int id,String t) {
		this.id = id;
		this.texto = t;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	
	
	
	

}
