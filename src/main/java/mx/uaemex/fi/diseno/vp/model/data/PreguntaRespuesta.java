package mx.uaemex.fi.diseno.vp.model.data;

public class PreguntaRespuesta {
	
	private Pregunta pregunta;
	private boolean respuesta;
	
	public PreguntaRespuesta() {
	
	}	
	
	public PreguntaRespuesta(Pregunta p, boolean r) {
		this.pregunta = p;
		this.respuesta = r;
		
	}

	public Pregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

	public boolean isRespuesta() {
		return respuesta;
	}

	public void setRespuesta(boolean respuesta) {
		this.respuesta = respuesta;
	}
	
	
	
	

}
