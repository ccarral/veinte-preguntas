package mx.uaemex.fi.ing_software_II.juegos;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mx.uaemex.fi.diseno.vp.model.ItemsDAO;
import mx.uaemex.fi.diseno.vp.model.ItemsDAODerbyImp;
import mx.uaemex.fi.diseno.vp.model.data.Item;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.derby.client.am.SqlException;

/**
 * Preparar el ambiente para la destrucción de pendientes
 */
@WebServlet("/veintePreguntas")
public class SilverSurfer extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private ItemsDAO itemsDao;
    private DataSource ds;
    private Random randomSource;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SilverSurfer() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		this.randomSource = new Random();
		try {
			ctx = new InitialContext();
			if(ctx != null) {
				this.ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/ds");
				if(this.ds == null) {
					throw new ServletException("DataSourceNotFound!");
				}
				
			}
		}catch(NamingException e) {
			throw new ServletException("No hay contexto inicial :(");
		}
	}



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<Item> items;
		Item item;
		ItemsDAODerbyImp dao = new ItemsDAODerbyImp();
		HttpSession sesion;
		try {
			dao.setConexion(this.ds.getConnection());
			this.itemsDao = dao;
			items = this.itemsDao.consultar();
			int val = this.randomSource.nextInt(items.size());
			item = items.get(val);
			sesion = request.getSession();
			sesion.setAttribute("objetivo", item);
			response.sendRedirect("consultarPreguntas");
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
