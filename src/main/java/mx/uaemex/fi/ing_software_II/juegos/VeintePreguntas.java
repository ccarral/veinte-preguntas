package mx.uaemex.fi.ing_software_II.juegos;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import mx.uaemex.fi.diseno.vp.model.PreguntasDAO;
import mx.uaemex.fi.diseno.vp.model.PreguntasDAODerbyImp;
import mx.uaemex.fi.diseno.vp.model.data.Pregunta;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.derby.client.am.SqlException;

/**
 * Servlet implementation class VeintePreguntas
 */
public class VeintePreguntas extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds;
	private PreguntasDAO dao;
       
	
    @Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			if(ctx != null) {
				this.ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/ds");
				if(this.ds == null) {
					throw new ServletException("DataSourceNotFound!");
				}
				
			}
		}catch(NamingException e) {
			throw new ServletException("No hay contexto inicial :(");
		}
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public VeintePreguntas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PreguntasDAODerbyImp realDao;
		PreguntasDAO dao;
		Connection con;
		realDao = new PreguntasDAODerbyImp();
		try {
			con = this.ds.getConnection();
			realDao.setConexion(con);
			dao = realDao;
			ArrayList<Pregunta> lista = dao.consultarPreguntas();
			con.close();
			response.getWriter().append(lista.get(0).toString());
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
