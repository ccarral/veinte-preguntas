package mx.uaemex.fi.ing_software_II.juegos;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebInitParam;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Servlet implementation class DaiLi
 */
public class DaiLi extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String enemigo;
	private ArrayList<String> enemigos = new ArrayList<>();
	

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		for(int i=1; i<8;i++) {
			String enemigo = config.getInitParameter("enemigo_"+i);
			System.out.println(enemigo);
			this.enemigos.add(enemigo);
		}
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer = response.getWriter();
		String usuario = request.getParameter("nombre");
		response.setContentType("text/html");
		writer.print("<!DOCTYPE html>"
				+ "<html>"
				+ "<head>"
				+ "<title> Ba Sing Se</title>"
				+ "</head>"
				+ "<body>");
		
		boolean esEnemigo = false;
		
		for(String e:this.enemigos) {
			if (usuario.equalsIgnoreCase(e)) {
				esEnemigo = true;
			}
		}
		
		if(!esEnemigo) {
			writer.print("<img src=\"img/blog-5.jpg\"/>"
					+ "<p>Hola "+usuario+"!</p>"
							+ "<h3>No hay guerra en Ba Sing Se</h3>");
		}else {
			writer.print("<img src=\"img/daiLi.png\"/>"
					+ "<p>Lo acompañamos a la salida</p>"
					+ "<h3>No hay guerra en Ba sing Se</3>");
		}
				
				
		writer.print("</body>"
				+ "</html>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
