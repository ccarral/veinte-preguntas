package mx.uaemex.fi.ing_software_II.juegos;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

/**
 * Servlet implementation class Naruto
 */
public class Naruto extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int limite;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Naruto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		Random random = new Random();
		int randInt = random.nextInt(this.limite);
		String texto = null;
		String numUsuarioStr = request.getParameter("numUsuario");
		int numUsuario = Integer.parseInt(numUsuarioStr);
		int ramenComidas = -1;
		
		HttpSession sesion = request.getSession();
		Object o = sesion.getAttribute("hmr");
		if(o != null) {
			ramenComidas = (Integer)o;
		}else {
			ramenComidas = random.nextInt(this.limite)+1;
			sesion.setAttribute("hmr", ramenComidas);
		}
		
		
//		Cookie[] cookies = request.getCookies();
//		
//		if(ramenComidas == -1) {
//			ramenComidas = random.nextInt(this.limite)+1;
//			Cookie c = new Cookie("hmr", ""+ramenComidas);
//			response.addCookie(c);
//		}
//		
//		if(cookies!=null) {
//			for(Cookie c:cookies) {
//				String n = c.getName();
//				if(n.compareToIgnoreCase("hmr") == 0) {
//					ramenComidas = random.nextInt(this.limite);
//				}
//			}
//		}

		
		if(numUsuario > ramenComidas) {
			texto = "No como tanto!";
		}else if(numUsuario < ramenComidas) {
			texto = "Me subestimas";
		}else if (numUsuario == ramenComidas){
			texto = "GANASTE!";
		}
		writer.print("<h1>"+texto+"</h1>");
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		String max = config.getInitParameter("maxNaruto");
		this.limite = Integer.parseInt(max);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
