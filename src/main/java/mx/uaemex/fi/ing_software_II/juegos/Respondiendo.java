package mx.uaemex.fi.ing_software_II.juegos;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import mx.uaemex.fi.diseno.vp.model.ItemsDAO;
import mx.uaemex.fi.diseno.vp.model.ItemsDAODerbyImp;
import mx.uaemex.fi.diseno.vp.model.PreguntasDAO;
import mx.uaemex.fi.diseno.vp.model.data.Item;
import mx.uaemex.fi.diseno.vp.model.data.Pregunta;
import mx.uaemex.fi.diseno.vp.model.data.PreguntaRespuesta;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class Respondiendo
 */
public class Respondiendo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ItemsDAO items;
	private DataSource ds;
	private PreguntasDAO dao;
	private Pregunta p;
	private Random r;
	private PreguntaRespuesta re;
	private Item coso;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Respondiendo() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		InitialContext ctx;
		this.r = new Random();
		try {
			ctx = new InitialContext();
			if(ctx != null) {
				this.ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/ds");
				if(this.ds == null) {
					throw new ServletException("DataSource not found");
				}
			}
		}catch(NamingException ne) {
			throw new ServletException("No hay contexto inicial :(");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pregunta = request.getParameter("pregunta");
		ItemsDAODerbyImp dao= new ItemsDAODerbyImp();
		ArrayList<Item>cosos;
		int val;

		Boolean resp;
		HttpSession sesion;
		this.p = new Pregunta(pregunta); 
		p.setTexto(pregunta);
		try{
			dao.setConexion(this.ds.getConnection());
			this.items = dao;
			cosos = this.items.consultar();
			sesion = request.getSession();
			coso = (Item) sesion.getAttribute("objetivo");
			System.err.println(coso);
			resp = this.items.consultarRespuesta(coso,p);
			sesion.setAttribute("respuesta", resp);
			response.sendRedirect("resultado.jsp");
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}



}
